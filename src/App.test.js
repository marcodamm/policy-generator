import { render, screen } from '@testing-library/react';
import App from './App';

test('renders compile button', () => {
  render(<App />);
  const linkElement = screen.getByText(/Text jetzt kompilieren/i);
  expect(linkElement).toBeInTheDocument();
});
