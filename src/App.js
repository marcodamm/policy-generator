import React, {Component} from 'react';
import {UncontrolledCollapse, Button, Col, Container, Form, FormGroup, Input, Label, Row, Nav, NavItem, NavLink, TabContent, TabPane} from 'reactstrap';
import MainMenu from './components/MainMenu/MainMenu';
import PolicySnippet from './components/PolicySnippet/PolicySnippet';
import ReactMarkdown from "react-markdown";
import Showdown from 'showdown';
import {Prism as SyntaxHighlighter} from 'react-syntax-highlighter';
import {PlusSquare, CheckCircleFill} from 'react-bootstrap-icons'
import {CopyToClipboard} from "react-copy-to-clipboard";
import gfm from 'remark-gfm';
import is from './utilities/type-checking';
import './App.css';

class App extends Component {

    constructor(props) {
        super(props);
        this.converter = new Showdown.Converter({tables: true});
    };

    state = {
        policyForm: {
            ceoName: "Martina Mustermann",
            ceoSalutation: "Frau",
            companyCity: "Musterhausen",
            companyName: "Muster AG",
            companyStreet: "Mustergasse 1",
            companyZipCode: "12345",
            contactEmail: "datenschutz@muster-ag.de",
            contactPhone: "0123 – 456 789 123",
            poSalutation: "Herr",
            poName: "Max Mustermann",
        },
        markdownOutput: {},
        compiledMarkdown: [],
        htmlOutput: "",
        activeTab: '1',
        codeOutputAvailable: false,
        copied: false,
        helpText: '',
        helpTextExample: ''
    };

    toggleTab = tab => {
        if(this.state.activeTab !== tab) this.setState({activeTab: tab});
    };

    policySources = [
        { title: 'preamble', url: 'https://bitbucket.org/wntrmt/markdown-templates/raw/HEAD/privacy-policy-preamble.md', },
        { title: 'personalData', url: 'https://bitbucket.org/wntrmt/markdown-templates/raw/HEAD/privacy-policy-personal-data.md', },
        { title: 'dataProcessingPurpose', url: 'https://bitbucket.org/wntrmt/markdown-templates/raw/HEAD/privacy-policy-data-processing-purpose.md', },
        { title: 'personalDataSharing', url: 'https://bitbucket.org/wntrmt/markdown-templates/raw/HEAD/privacy-policy-personal-data-sharing.md', },
        { title: 'additionalFunctionality', url: 'https://bitbucket.org/wntrmt/markdown-templates/raw/HEAD/privacy-policy-additional-functionality.md', },
        { title: 'googleAnalytics', url: 'https://bitbucket.org/wntrmt/markdown-templates/raw/HEAD/privacy-policy-google-analytics.md', },
        { title: 'socialNetworkLinks', url: 'https://bitbucket.org/wntrmt/markdown-templates/raw/HEAD/privacy-policy-social-network-links.md', },
        { title: 'googleTagManager', url: 'https://bitbucket.org/wntrmt/markdown-templates/raw/HEAD/privacy-policy-google-tag-manager.md', },
        { title: 'googleRecaptcha', url: 'https://bitbucket.org/wntrmt/markdown-templates/raw/HEAD/privacy-policy-google-recaptcha.md', },
        { title: 'etracker', url: 'https://bitbucket.org/wntrmt/markdown-templates/raw/HEAD/privacy-policy-etracker.md', },
        { title: 'adobeFonts', url: 'https://bitbucket.org/wntrmt/markdown-templates/raw/HEAD/privacy-policy-adobe-fonts.md', },
        { title: 'googleFonts', url: 'https://bitbucket.org/wntrmt/markdown-templates/raw/HEAD/privacy-policy-google-fonts.md', },
        { title: 'storageDuration', url: 'https://bitbucket.org/wntrmt/markdown-templates/raw/HEAD/privacy-policy-storage-duration.md', },
        { title: 'obligatoryDataProvision', url: 'https://bitbucket.org/wntrmt/markdown-templates/raw/HEAD/privacy-policy-obligatory-data-provision.md', },
        { title: 'profiling', url: 'https://bitbucket.org/wntrmt/markdown-templates/raw/HEAD/privacy-policy-profiling.md', },
        { title: 'userRights', url: 'https://bitbucket.org/wntrmt/markdown-templates/raw/HEAD/privacy-policy-user-rights.md', },
        { title: 'optOutRights', url: 'https://bitbucket.org/wntrmt/markdown-templates/raw/HEAD/privacy-policy-opt-out-rights.md', },
        { title: 'browserOptOutSettings', url: 'https://bitbucket.org/wntrmt/markdown-templates/raw/HEAD/privacy-policy-browser-opt-out-settings.md', },
        { title: 'cookieTable', url: 'https://bitbucket.org/wntrmt/markdown-templates/raw/HEAD/privacy-policy-cookie-table.md', },
        { title: 'addendum', url: 'https://bitbucket.org/wntrmt/markdown-templates/raw/HEAD/privacy-policy-addendum.md', },
    ];

    componentDidMount() {

        this.policySources.map((policySource, index) => fetch(policySource.url)
            .then(response => {
                return response.text()
            })
            .then((text) => {
                this.setState((prevState => ({
                    ...prevState,
                    markdownOutput: {
                        ...prevState.markdownOutput,
                        [policySource.title]: {
                            title: policySource.title,
                            order: index,
                            document: text,
                            url: policySource.url
                        }
                    }
                })))
            })
        );
    }

    cloneArray = (originalArray = []) => {
        const suppressError = true;
        if (!is.anArray(originalArray))
            return;
        return originalArray.map(element => {
            if (React.isValidElement(element))
                return element; // valid React elements are pushed to the new array as-is
            if (is.anObject(element, suppressError))
                return this.cloneObject(element); // push the CLONED object to the new array
            if (is.anArray(element, suppressError))
                return this.cloneArray(element);  // push the CLONED array to the new array
            return element;  // if it's neither an array nor an object, just push it to the new array
        });
    };

    cloneObject = (originalObject = {}) => {
        const suppressError = true;
        if (!is.anObject(originalObject))
            return;
        let clonedObject = {};
        Object.keys(originalObject).forEach(key => {
            const currentValue = originalObject[key];
            if (React.isValidElement(currentValue))
                clonedObject[key] = currentValue; // valid React elements are added to the new object as-is
            else if (is.anObject(currentValue, suppressError))
                clonedObject[key] = this.cloneObject(currentValue);  // set this key to the CLONED object
            else if (is.anArray(currentValue, suppressError))
                clonedObject[key] = this.cloneArray(currentValue);  // set this key to the CLONED array
            else
                clonedObject[key] = currentValue;  // if it's neither an object nor an array, just set this key to the value
        });
        return clonedObject;
    };

    onFocusHandler = (event, helpTitle) => {
        switch(event.target.name) {
            case('companyName'):
                console.log(event.target);
                this.setState({
                    helpTitle: helpTitle,
                    helpText: 'Der vollständige Name der datenverantwortlichen Unternehmung',
                    helpTextExample: 'Muster AG'
                });
                break;
            case('companyStreet'):
                this.setState({
                    helpTitle: helpTitle,
                    helpText: 'Adresse der datenverantwortlichen Unternehmung (Straße)',
                    helpTextExample: 'Musterstraße 321'
                });
                break;
            case('companyZipCode'):
                this.setState({
                    helpTitle: helpTitle,
                    helpText: 'Adresse der datenverantwortlichen Unternehmung (Postleitzahl)',
                    helpTextExample: '54321'
                });
                break;
            case('companyCity'):
                this.setState({
                    helpTitle: helpTitle,
                    helpText: 'Adresse der datenverantwortlichen Unternehmung (Ort)',
                    helpTextExample: 'Musterhausen'
                });
                break;
            case('contactPhone'):
                this.setState({
                    helpTitle: helpTitle,
                    helpText: 'Telefonnummer unter welcher die datenverantwortliche Unternehmung Anfragen zum Datenschutz entgegennimmt',
                    helpTextExample: '0123 – 456 789 123'
                });
                break;
            case('contactEmail'):
                this.setState({
                    helpTitle: helpTitle,
                    helpText: 'E-Mail-Adresse unter welcher die datenverantwortliche Unternehmung Anfragen zum Datenschutz entgegennimmt',
                    helpTextExample: 'datenschutz@muster-ag.de'
                });
                break;
            case('ceoName'):
                this.setState({
                    helpTitle: helpTitle,
                    helpText: 'Vollständiger Name der vertretungsberechtigten Geschäftsführung der Unternehmung',
                    helpTextExample: 'Martina Mustermann'
                });
                break;
            case('poName'):
                this.setState({
                    helpTitle: helpTitle,
                    helpText: 'Vollständiger Name des verantwortlichen Datenschutzbeauftragten der Unternehmung',
                    helpTextExample: 'Max Mustermann'
                });
                break;
            default:
                this.setState({
                    helpTitle: 'Ohne Titel',
                    helpText: 'Für diesen Eintrag ist kein Hilfetext hinterlegt',
                    helpTextExample: 'Hier gibt\'s nichts zu sehen'
                });
                break;
        }
    };

    onClickHandler = (event, inputIdentifier) => {
        console.log(event)
        console.log(inputIdentifier)
    }

    onChangeHandler = (event, inputIdentifier) => {
        const updatedProjectForm = {
            ...this.state.policyForm
        };

        if (event.target.type === 'checkbox') {
            console.log(event.target);
            updatedProjectForm[inputIdentifier] = event.target.checked;
            if (event.target.checked) {
                this.setState((prevState => ({
                    compiledMarkdown: [...prevState.compiledMarkdown, this.state.markdownOutput[event.target.name]]
                })))
            } else {
                let arr = this.state.compiledMarkdown;
                arr = arr.filter(item => {
                    return item.title !== event.target.name
                });
                this.setState({compiledMarkdown: arr})
            }
        } else {
            updatedProjectForm[inputIdentifier] = event.target.value;
        }

        this.setState({policyForm: updatedProjectForm});
    };

    onCompileHandler = (event) => {

        event.preventDefault();
        const clonedSelection = this.cloneArray(this.state.compiledMarkdown);
        const clonedMarkdown = this.cloneObject(this.state.markdownOutput);

        const mapObj = {
            RPL_COMPANY_NAME: this.state.policyForm.companyName,
            RPL_COMPANY_STREET: this.state.policyForm.companyStreet,
            RPL_COMPANY_ZIPCODE: this.state.policyForm.companyZipCode,
            RPL_COMPANY_CITY: this.state.policyForm.companyCity,
            RPL_CONTACT_PHONE: this.state.policyForm.contactPhone,
            RPL_CONTACT_EMAIL: this.state.policyForm.contactEmail,
            RPL_CEO_SALUTATION: this.state.policyForm.ceoSalutation,
            RPL_CEO_NAME: this.state.policyForm.ceoName,
            RPL_PO_SALUTATION: this.state.policyForm.poSalutation,
            RPL_PO_NAME: this.state.policyForm.poName
        };

        // update rendered markdown output by replacing placeholders with user generated variables (form input values)
        let re = new RegExp(Object.keys(mapObj).join("|"),"gi");
        const loopNestedObj = clonedMarkdown => {
            Object.keys(clonedMarkdown).forEach(key => {
                if (clonedMarkdown[key] && typeof clonedMarkdown[key] === "object") { // recurse
                    loopNestedObj(clonedMarkdown[key]);
                } else {
                    clonedMarkdown.document = clonedMarkdown.document.replace(re, function(matched) {
                        return mapObj[matched];
                    });
                }
            });
        };
        loopNestedObj(clonedMarkdown);

        let HTMLOutput = `
            \n${clonedMarkdown.preamble.document}
            \n${clonedMarkdown.personalData.document}
            \n${clonedMarkdown.dataProcessingPurpose.document}
            \n${clonedMarkdown.personalDataSharing.document}
            \n${clonedMarkdown.additionalFunctionality.document}
        `;

        clonedSelection.sort((a, b) => {
            return a.order - b.order
        });
        clonedSelection.forEach((selectionItem) => {
            HTMLOutput += `\n${selectionItem.document}`;
        });

        HTMLOutput += `
            \n${clonedMarkdown.storageDuration.document}
            \n${clonedMarkdown.obligatoryDataProvision.document}
            \n${clonedMarkdown.profiling.document}
            \n${clonedMarkdown.userRights.document}
            \n${clonedMarkdown.optOutRights.document}
            \n${clonedMarkdown.browserOptOutSettings.document}
            \n${clonedMarkdown.cookieTable.document}
            \n${clonedMarkdown.addendum.document}
        `;

        // Extend logic to update everything and not just preamble
        this.setState({markdownOutput: clonedMarkdown}, () => {
            this.setState({
                htmlOutput: this.converter.makeHtml(HTMLOutput),
                codeOutputAvailable: true
            });
        })
    };

    render() {

        const optionalPolicies = [];

        this.policySources.forEach((policySource, key) => {
            if(this.state.policyForm[policySource.title]) {
                optionalPolicies.push(
                    <PolicySnippet
                        key={key}
                        color="primary"
                        onClick={(event) => this.onClickHandler(event, policySource.title)}
                        text={<ReactMarkdown disallowedTypes={[]} source={this.state.markdownOutput[policySource.title].document} />}/>
                );
            }
        })

        let preamble = "";
        let personalData = "";
        let dataProcessingPurpose = "";
        let personalDataSharing = "";
        let additionalFunctionality = "";
        let storageDuration = "";
        let obligatoryDataProvision = "";
        let profiling = "";
        let userRights = "";
        let optOutRights = "";
        let browserOptOutSettings = "";
        let cookieTable = "";
        let addendum = "";

        if (this.state.markdownOutput.preamble)                { preamble              = <ReactMarkdown disallowedTypes={[]} source={this.state.markdownOutput.preamble.document} />; }
        if (this.state.markdownOutput.personalData)            { personalData          = <ReactMarkdown disallowedTypes={[]} source={this.state.markdownOutput.personalData.document} />; }
        if (this.state.markdownOutput.dataProcessingPurpose)   { dataProcessingPurpose = <ReactMarkdown disallowedTypes={[]} source={this.state.markdownOutput.dataProcessingPurpose.document} />; }
        if (this.state.markdownOutput.personalDataSharing)     { personalDataSharing   = <ReactMarkdown plugins={[gfm]} disallowedTypes={[]} source={this.state.markdownOutput.personalDataSharing.document} />; }
        if (this.state.markdownOutput.additionalFunctionality) { additionalFunctionality = <ReactMarkdown disallowedTypes={[]} source={this.state.markdownOutput.additionalFunctionality.document} />; }
        if (this.state.markdownOutput.storageDuration)         { storageDuration       = <ReactMarkdown disallowedTypes={[]} source={this.state.markdownOutput.storageDuration.document} />; }
        if (this.state.markdownOutput.obligatoryDataProvision) { obligatoryDataProvision = <ReactMarkdown disallowedTypes={[]} source={this.state.markdownOutput.obligatoryDataProvision.document} />; }
        if (this.state.markdownOutput.profiling)               { profiling             = <ReactMarkdown disallowedTypes={[]} source={this.state.markdownOutput.profiling.document} />; }
        if (this.state.markdownOutput.userRights)              { userRights            = <ReactMarkdown disallowedTypes={[]} source={this.state.markdownOutput.userRights.document} />; }
        if (this.state.markdownOutput.optOutRights)            { optOutRights          = <ReactMarkdown disallowedTypes={[]} source={this.state.markdownOutput.optOutRights.document} />; }
        if (this.state.markdownOutput.browserOptOutSettings)   { browserOptOutSettings = <ReactMarkdown disallowedTypes={[]} source={this.state.markdownOutput.browserOptOutSettings.document} />; }
        if (this.state.markdownOutput.cookieTable)             { cookieTable           = <ReactMarkdown plugins={[gfm]} disallowedTypes={[]} source={this.state.markdownOutput.cookieTable.document} />; }
        if (this.state.markdownOutput.addendum)                {addendum               = <ReactMarkdown disallowedTypes={[]} source={this.state.markdownOutput.addendum.document} />; }

        // add css classes to trigger animation on codeOutputAvailable (i.e. after compilation)
        let codeOutputClasses = () => {
            if (this.state.activeTab === '1' && this.state.codeOutputAvailable) {
                return 'output__code highlight'
            } else if (this.state.activeTab === '1' && !this.state.codeOutputAvailable) {
                return ''
            }
        };

        return (
            <Container className="App" fluid>
                <Row className="mb-3">
                    <Col>
                        <MainMenu/>
                    </Col>
                </Row>
                <Row className="mb-3">

                    <Col md={3}>
                        <Form className="sticky-top">
                            <legend type="button" id="policyForm" className="d-flex justify-content-between">Kontaktdaten des Verantwortlichen <PlusSquare/></legend>
                            <UncontrolledCollapse toggler="#policyForm">
                                <FormGroup row>
                                    <Col xs={12}>
                                        <Label for="responsibleEntity">Datenverantwortlicher</Label>
                                        <Input
                                            disabled
                                            onChange={(event) => this.onChangeHandler(event, "responsibleEntity")}
                                            defaultValue=""
                                            type="select"
                                            name="responsibleEntity"
                                            id="responsibleEntity">
                                            <option hidden value="">Bitte wählen</option>
                                            <option value="preamble">Ich bin verantwortlich</option>
                                            <option value="thirdParty">Ein Dritter ist verantwortlich</option>
                                        </Input>
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Col xs={12}>
                                        <Label for="companyName">Vollständiger Firmenname</Label>
                                        <Input
                                            defaultValue={this.state.companyName}
                                            onChange={(event) => this.onChangeHandler(event, "companyName")}
                                            onFocus={(event) => this.onFocusHandler(event, "Vollständiger Firmenname")}
                                            type="text" name="companyName" id="companyName"
                                            placeholder="Muster GmbH"/>
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Col xs={12}>
                                        <Label for="companyStreet">Straße und Hausnummer</Label>
                                        <Input
                                            defaultValue={this.state.companyStreet}
                                            onChange={(event) => this.onChangeHandler(event, "companyStreet")}
                                            onFocus={(event) => this.onFocusHandler(event, "Adresse - Straße")}
                                            type="text" name="companyStreet" id="companyStreet"
                                            placeholder="Musterstraße 1"/>
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Col xs={12} lg={4}>
                                        <Label for="companyZipCode">Postleitzahl</Label>
                                        <Input
                                            defaultValue={this.state.companyZipCode}
                                            onChange={(event) => this.onChangeHandler(event, "companyZipCode")}
                                            onFocus={(event) => this.onFocusHandler(event, "Adresse - Postleitzahl")}
                                            type="text" name="companyZipCode" id="companyZipCode"
                                            placeholder="54321"/>
                                    </Col>
                                    <Col xs={12} lg={8}>
                                        <Label for="companyCity">Ort</Label>
                                        <Input
                                            defaultValue={this.state.companyCity}
                                            onChange={(event) => this.onChangeHandler(event, "companyCity")}
                                            onFocus={(event) => this.onFocusHandler(event, "Adresse - Ort")}
                                            type="text" name="companyCity" id="companyCity"
                                            placeholder="Musterhausen"/>
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Col xs={12} lg={4}>
                                        <Label for="contactPhone">Telefonnummer</Label>
                                        <Input
                                            defaultValue={this.state.contactPhone}
                                            onChange={(event) => this.onChangeHandler(event, "contactPhone")}
                                            onFocus={(event) => this.onFocusHandler(event, "Telefonnumer")}
                                            type="text" name="contactPhone" id="contactPhone"
                                            placeholder="0123 456 789"/>
                                    </Col>
                                    <Col xs={12} lg={8}>
                                        <Label for="contactEmail">E-Mail-Adresse</Label>
                                        <Input
                                            defaultValue={this.state.contactEmail}
                                            onChange={(event) => this.onChangeHandler(event, "contactEmail")}
                                            onFocus={(event) => this.onFocusHandler(event, "E-Mail-Adresse")}
                                            type="text" name="contactEmail" id="contactEmail"
                                            placeholder="datenschutz@mustergmbh.de"/>
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Col xs={12} lg={4}>
                                        <Label for="ceoSalutation">Anrede GF</Label>
                                        <Input
                                            defaultValue={this.state.ceoSalutation}
                                            onChange={(event) => this.onChangeHandler(event, "ceoSalutation")}
                                            onFocus={(event) => this.onFocusHandler(event, "Anrede Geschäftsführung")}
                                            type="text" name="ceoSalutation" id="ceoSalutation"
                                            placeholder="Frau"/>
                                    </Col>
                                    <Col xs={12} lg={8}>
                                        <Label for="ceoName">Vollständiger Name GF</Label>
                                        <Input
                                            defaultValue={this.state.ceoName}
                                            onChange={(event) => this.onChangeHandler(event, "ceoName")}
                                            onFocus={(event) => this.onFocusHandler(event, "Vollständiger Name Geschäftsführung")}
                                            type="text" name="ceoName" id="ceoName"
                                            placeholder="Martina Mustermann"/>
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Col xs={12} lg={4}>
                                        <Label for="poSalutation">Anrede DSB</Label>
                                        <Input
                                            defaultValue={this.state.poSalutation}
                                            onChange={(event) => this.onChangeHandler(event, "poSalutation")}
                                            onFocus={(event) => this.onFocusHandler(event, "Anrede Datenschutzbeauftrage:r")}
                                            type="text" name="poSalutation" id="poSalutation"
                                            placeholder="Herr"/>
                                    </Col>
                                    <Col xs={12} lg={8}>
                                        <Label for="poName">Vollständiger Name DSB</Label>
                                        <Input
                                            defaultValue={this.state.poName}
                                            onChange={(event) => this.onChangeHandler(event, "poName")}
                                            onFocus={(event) => this.onFocusHandler(event, "Vollständiger Name Datenschutzbeauftragte:r")}
                                            type="text" name="poName" id="poName"
                                            placeholder="Max Mustermann"/>
                                    </Col>
                                </FormGroup>
                            </UncontrolledCollapse>

                            <legend type="button" id="webanalysis" className="d-flex justify-content-between">Web-Analyse und Tracking <PlusSquare/></legend>
                            <UncontrolledCollapse toggler="#webanalysis">
                                <FormGroup row check>
                                    <Col xs={12}>
                                        <Input type="checkbox" name="googleAnalytics" id="googleAnalytics"
                                               onChange={(event) => this.onChangeHandler(event, "googleAnalytics")}
                                        />
                                        <Label for="googleAnalytics" check>Wir nutzen Google Analytics</Label>
                                    </Col>
                                </FormGroup>
                                <FormGroup row check>
                                    <Col xs={12}>
                                        <Input type="checkbox" name="googleTagManager" id="googleTagManager"
                                               onChange={(event) => this.onChangeHandler(event, "googleTagManager")}
                                        />
                                        <Label for="googleTagManager" check>Wir nutzen Google Tag Manager</Label>
                                    </Col>
                                </FormGroup>
                                <FormGroup row check>
                                    <Col xs={12}>
                                        <Input type="checkbox" name="etracker" id="etracker"
                                               onChange={(event) => this.onChangeHandler(event, "etracker")}
                                        />
                                        <Label for="etracker" check>Wir nutzen eTracker</Label>
                                    </Col>
                                </FormGroup>
                            </UncontrolledCollapse>

                            <legend type="button" id="itsecurity" className="d-flex justify-content-between">IT-Sicherheits-Tools <PlusSquare/></legend>
                            <UncontrolledCollapse toggler="#itsecurity">
                                <FormGroup row check>
                                    <Col xs={12}>
                                        <Input type="checkbox" name="googleRecaptcha" id="googleRecaptcha"
                                               onChange={(event) => this.onChangeHandler(event, "googleRecaptcha")}
                                        />
                                        <Label for="googleRecaptcha" check>Wir nutzen Google reCAPTCHA</Label>
                                    </Col>
                                </FormGroup>
                                <FormGroup row check>
                                    <Col xs={12}>
                                        <Input type="checkbox" name="cloudflareHcaptcha" id="cloudflareHcaptcha" disabled
                                               onChange={(event) => this.onChangeHandler(event, "cloudflareHcaptcha")}
                                        />
                                        <Label for="cloudflareHcaptcha" check>Wir nutzen Cloudflare hCaptcha</Label>
                                    </Col>
                                </FormGroup>
                            </UncontrolledCollapse>

                            <legend type="button" id="fontservices" className="d-flex justify-content-between">Schriftartendienste <PlusSquare/></legend>
                            <UncontrolledCollapse toggler="#fontservices">
                                <FormGroup row check>
                                    <Col xs={12}>
                                        <Input type="checkbox" name="adobeFonts" id="adobeFonts"
                                               onChange={(event) => this.onChangeHandler(event, "adobeFonts")}
                                        />
                                        <Label for="adobeFonts" check>Wir nutzen Adobe Fonts</Label>
                                    </Col>
                                </FormGroup>
                                <FormGroup row check>
                                    <Col xs={12}>
                                        <Input type="checkbox" name="googleFonts" id="googleFonts"
                                               onChange={(event) => this.onChangeHandler(event, "googleFonts")}
                                        />
                                        <Label for="googleFonts" check>Wir nutzen Google Fonts</Label>
                                    </Col>
                                </FormGroup>
                                <FormGroup row check>
                                    <Col xs={12}>
                                        <Input type="checkbox" name="fontAwesome" id="fontAwesome" disabled
                                               onChange={(event) => this.onChangeHandler(event, "fontAwesome")}
                                        />
                                        <Label for="fontAwesome" check>Wir nutzen Font Awesome Fonts</Label>
                                    </Col>
                                </FormGroup>
                            </UncontrolledCollapse>

                            <legend id="socialnetworks" type="button" className="d-flex justify-content-between">Soziale Netzwerke <PlusSquare/></legend>
                            <UncontrolledCollapse toggler="#socialnetworks">
                                <FormGroup row check>
                                    <Col xs={12}>
                                        <Input type="checkbox" name="socialNetworkLinks" id="socialNetworkLinks"
                                               onChange={(event) => this.onChangeHandler(event, "socialNetworkLinks")}
                                        />
                                        <Label for="trackingSocialLinks" check>Wir verlinken auf Soziale Netzwerke</Label>
                                    </Col>
                                </FormGroup>
                            </UncontrolledCollapse>
                                <Button
                                    block
                                    color="primary"
                                    onClick={this.onCompileHandler}
                                    className="mt-4"
                                >Text jetzt kompilieren</Button>
                                <Button block color="primary" outline>Alles zurücksetzen</Button>
                        </Form>
                    </Col>

                    <Col md={6}>
                        <Nav tabs>
                            <NavItem>
                                <NavLink
                                    className={this.state.activeTab !== '1' ? 'text-dark' : 'active'}
                                    onClick={() => { this.toggleTab('1'); }}>
                                    Formatierter Text
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    className={codeOutputClasses()}
                                    disabled={!this.state.codeOutputAvailable}
                                    onClick={() => { this.toggleTab('2'); }}>
                                    Code-Ausgabe {
                                        this.state.codeOutputAvailable ?
                                        <CheckCircleFill id="checkmark"/> :
                                        <CheckCircleFill id="checkmark"/>
                                    }
                                </NavLink>
                            </NavItem>
                        </Nav>
                        <TabContent activeTab={this.state.activeTab}>
                            <TabPane tabId="1">
                                <Row>
                                    <Col>
                                        {preamble}
                                        {personalData}
                                        {dataProcessingPurpose}
                                        {personalDataSharing}
                                        {additionalFunctionality}
                                        {optionalPolicies}
                                        {storageDuration}
                                        {obligatoryDataProvision}
                                        {profiling}
                                        {userRights}
                                        {optOutRights}
                                        {browserOptOutSettings}
                                        {cookieTable}
                                        {addendum}
                                    </Col>
                                </Row>
                            </TabPane>
                            <TabPane tabId="2">
                                <Row className="my-2">
                                    <Col>
                                    <CopyToClipboard text={this.state.htmlOutput}
                                                     onCopy={() => this.setState({copied: true})}>

                                        {this.state.copied ? <Button color="success">In Zwischenablage kopiert!</Button> : <Button color="primary">In Zwischenablage kopieren</Button>}
                                    </CopyToClipboard>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <SyntaxHighlighter
                                            language="markup"
                                            showLineNumbers>
                                            {this.state.htmlOutput}
                                        </SyntaxHighlighter>
                                    </Col>
                                </Row>
                            </TabPane>
                        </TabContent>
                    </Col>
                    <Col md={3}>
                        <div className="sticky-top">
                            <h3>Hilfe</h3>
                            <h4>{this.state.helpTitle}</h4>
                            <p>{this.state.helpText}</p>
                            <SyntaxHighlighter>{this.state.helpTextExample}</SyntaxHighlighter>
                        </div>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default App;
