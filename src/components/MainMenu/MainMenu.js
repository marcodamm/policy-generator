import React from 'react';
import {CodeSquare} from 'react-bootstrap-icons';
import {
    Navbar,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink
} from "reactstrap";

const MainMenu = () => {
    return (
        <Navbar color="dark" dark expand="md">
            <NavbarBrand href="/">PolicyGenerator</NavbarBrand>
            <Nav className="mr-auto" navbar>
                <NavItem>
                    <NavLink target="_blank" href="https://gitlab.com/marcodamm/policy-generator"><CodeSquare id="repositoryLink"/> GitLab</NavLink>
                </NavItem>
            </Nav>
        </Navbar>
    )
};

export default MainMenu;
